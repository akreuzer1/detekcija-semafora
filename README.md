# Detekcija semafora



Ovaj kod demonstrira detekciju semafora na videu koristeći OpenCV biblioteku. Kod čita video datoteku "DayDrive2.mp4" i primjenjuje bojeni prostor HSV kako bi prepoznao semafore u slici.

# Instalacija
Prvo je potrebno instalirati OpenCV biblioteku. Možete je instalirati koristeći pip naredbu:
pip install opencv-python
Preuzmite kod i spremite ga u željeni direktorij.

# Korištenje
Učitavanje videa:

Provjerite da se video datoteka "DayDrive2.mp4" nalazi u istom direktoriju kao i kod.
Ako želite koristiti drugu video datoteku, promijenite putanju u liniji koda:
cap = cv.VideoCapture("DayDrive2.mp4")

# Pokretanje programa:

Otvorite terminal i navigirajte do direktorija u kojem se nalazi kod.
Pokrenite program pomoću sljedeće naredbe:
python ime_datoteke.py

# Kontrole tijekom izvođenja:

Prikazan je prozor s videom, na kojem su označeni prepoznati semafori kvadratima.
Kako biste prekinuli izvođenje programa, pritisnite tipku "q".
Za premotavanje unatrag za 10 frejmova, pritisnite tipku "r".
Za premotavanje unaprijed za 150 frejmova, pritisnite tipku "f".
Za premotavanje unatrag za 150 frejmova, pritisnite tipku "d".

# Napomene
Bojeni prostor HSV koristi se za prepoznavanje crvene, žute i zelene boje semafora. Parametri boja definirani su u varijabli hsv_ranges.
ROI (Region of Interest) definiran je kao gornja polovica slike. Ako želite promijeniti ROI, prilagodite indekse izrezivanja u liniji koda:

roi = frame[100:frame.shape[0]//1, 0:frame.shape[1]]

Semafori se označavaju na slici prepoznavanjem kvadratnih kontura.
Filtriranje kontura prema površini vrši se provjerom površine konture. Trenutno su postavljene granice na 500 i 3000.
Detektirani semafori označavaju se na slici kvadratima zelene boje, zajedno s natpisom "Semafor".
Obratite pozornost na postavke kontrole tijekom izvođenja programa kako biste mogli navigirati kroz video.
Molimo vas da imate na umu da ovaj kod trenutno radi samo za detekciju semafora u videu "DayDrive2
