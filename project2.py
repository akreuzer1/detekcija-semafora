import cv2 as cv
import numpy as np

# Parametri bojenog prostora HSV za crveni, žuti i zeleni semafor
hsv_ranges = [
    ((0, 100, 100), (10, 255, 255)),   # Crvena boja
    ((20, 100, 100), (40, 255, 255)),  # Žuta boja
    ((50, 100, 100), (163, 255, 255))  # Zelena boja
]

cap = cv.VideoCapture("DayDrive2.mp4")
frame_pos = 0

while(cap.isOpened()):
    cap.set(cv.CAP_PROP_POS_FRAMES, frame_pos)
    ret, frame = cap.read()

    if ret:
        # Izdvajanje ROI-aitit
        roi = frame[50:frame.shape[0]//1, 0:frame.shape[1]]

        # Konverzija u HSV bojeni prostor
        hsv = cv.cvtColor(roi, cv.COLOR_BGR2HSV)

        # Prepoznavanje semafora
        detected_squares = []
        for i, (lower, upper) in enumerate(hsv_ranges):
            mask = cv.inRange(hsv, lower, upper)
            contours, _ = cv.findContours(mask, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)

            for contour in contours:
                area = cv.contourArea(contour)
                if 500 < area < 3000: # Filtriranje kontura prema površini

                    # Aproksimacija konture na pravokutnik
                    epsilon = 0.1 * cv.arcLength(contour, True)
                    approx = cv.approxPolyDP(contour, epsilon, True)

                    if len(approx) == 4:  # Provjera je li kontura kvadratna
                        detected_squares.append(approx)

        # Označavanje semafora na slici
        for square in detected_squares:
            x, y, w, h = cv.boundingRect(square)
            cv.rectangle(roi, (x, y), (x + w, y + h), (0, 255, 0), 2)
            cv.putText(roi, "Semafor", (x, y - 10), cv.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

        # Prikazivanje slike
        cv.imshow('Frame', roi)

        # Provjera za izlazak iz petlje
        key = cv.waitKey(25)
        if key & 0xFF == ord('q'):
            break
        elif key & 0xFF == ord('r'):
            frame_pos = max(frame_pos - 10, 0)  # rewind by 10 frames
        elif key & 0xFF == ord('f'):
            frame_pos += 150  # forward by 150 frames
        elif key & 0xFF == ord('d'):
            frame_pos -= 150  # forward by 150 frames
    else:
        break

    frame_pos += 1

cap.release()
cv.destroyAllWindows()





